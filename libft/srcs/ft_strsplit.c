/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 07:08:37 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:36:48 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	**ft_strsplit(char const *s, char c)
{
	unsigned int	i;
	unsigned int	len;
	char			**p;

	i = 0;
	len = ft_tokenlen(s, c);
	if (!s || !(p = (char **)malloc((len + 1) * sizeof(char *))))
		return (NULL);
	while (i < len)
	{
		s = ft_strskip(s, c);
		if (!(p[i] = ft_strsub(s, 0, ft_strcspn(s, &c))))
			return (NULL);
		s += ft_strcspn(s, &c);
		i++;
	}
	p[len] = NULL;
	return (p);
}
