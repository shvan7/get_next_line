/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 13:47:55 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 15:29:29 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *new;
	t_list *save;

	if (!lst || !f)
		return (NULL);
	new = (*f)(lst);
	save = new;
	while (lst->next)
	{
		lst = lst->next;
		new->next = (*f)(lst);
		new = new->next;
	}
	return (save);
}
