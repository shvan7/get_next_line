/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 05:08:00 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:32:03 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *str, const char *find)
{
	int i;
	int j;

	i = 0;
	j = 0;
	if (find[0] == '\0')
		return (char *)(str);
	while (str[i] != find[j] && str[i])
		i++;
	while (str[i])
	{
		while (str[i] == find[j] && str[i] && find[j])
		{
			if (find[j + 1] == '\0')
				return (char *)(str + i - j);
			i++;
			j++;
		}
		i = i - j + 1;
		j = 0;
	}
	return (NULL);
}
