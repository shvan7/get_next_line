/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 06:43:00 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:35:17 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strtrim(char const *s)
{
	unsigned int a;
	unsigned int z;

	if (!s)
		return (NULL);
	a = 0;
	z = ft_strlen(s);
	z = z ? z - 1 : 0;
	while (s[a] && ft_istrim(s[a]))
		a++;
	while (z > 0 && ft_istrim(s[z]))
		z--;
	if (a >= z)
		return (ft_strnew(1));
	return (ft_strsub(s, a, (z - a + 1)));
}
