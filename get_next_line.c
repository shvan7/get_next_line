/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 16:48:41 by aulima-f          #+#    #+#             */
/*   Updated: 2018/12/05 13:57:08 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static t_lstgnl			*ft_lstfind_fd(t_lstgnl **data, int fd)
{
	t_lstgnl *tmp;

	tmp = data[0];
	while (tmp)
	{
		if (tmp->fd == fd)
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

static t_lstgnl			*ft_newlst_gnl(t_lstgnl **data, int fd)
{
	t_lstgnl *list;

	if (!(list = (t_lstgnl *)ft_memalloc(sizeof(t_lstgnl))))
		return (NULL);
	list->content = NULL;
	list->fd = fd;
	list->end = 1;
	list->next = data[0];
	data[0] = list;
	return (list);
}

static int				ft_read_gnl(int fd, char **str)
{
	char	*buff;
	char	*p;
	int		i;

	if (!(buff = (char *)ft_strnew(BUFF_SIZE)))
		return (-1);
	while (!(ft_strchr(buff, '\n')) && ((i = read(fd, buff, BUFF_SIZE)) > 0))
	{
		ft_remchar(buff, '\0', i);
		p = *str;
		if (!(*str = ft_strjoin(p, buff))
				&& !(*str = ft_strdup(buff)))
			return (-1);
		ft_strdel(&p);
	}
	ft_strdel(&buff);
	return (i);
}

static int				ft_before_read(int fd, t_lstgnl **data)
{
	t_lstgnl	*list;
	int			i;
	int			len;
	char		*p;

	if ((list = ft_lstfind_fd(data, fd)) && list->content)
	{
		i = ft_strcspn(list->content, "\n") + 1;
		p = list->content;
		len = ft_strlen(p);
		if (i < len)
			if (!(list->content = ft_strsub(p, i, len - i)))
				return (-1);
		if (i == len)
			list->content = NULL;
		ft_strdel(&p);
		if (!list->end)
			return (0);
	}
	else
		list = ft_newlst_gnl(data, fd);
	return ((list->end = ft_read_gnl(fd, &list->content)));
}

int						get_next_line(const int fd, char **line)
{
	static t_lstgnl	**data;
	t_lstgnl		*stock;
	int				i;

	if (!data)
	{
		if (!(data = (t_lstgnl **)malloc(sizeof(t_lstgnl))))
			return (-1);
		data[0] = NULL;
	}
	if (fd < 0 || !line || (ft_before_read(fd, data) == -1))
		return (-1);
	stock = ft_lstfind_fd(data, fd);
	i = ft_strcspn(stock->content, "\n");
	if (!stock->end && i < 0)
	{
		*line = ft_strdup(stock->content);
		ft_strdel(&stock->content);
	}
	else
		*line = ft_strsub(stock->content, 0, i);
	return (!!*line);
}
