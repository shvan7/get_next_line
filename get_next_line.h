/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 16:49:12 by aulima-f          #+#    #+#             */
/*   Updated: 2018/12/05 16:37:43 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 1

# include <unistd.h>
# include <stdlib.h>
# include "libft.h"

typedef struct		s_lstgnl
{
	char			*content;
	int				fd;
	int				end;
	struct s_lstgnl	*next;
}					t_lstgnl;

int					get_next_line(const int fd, char **line);

#endif
